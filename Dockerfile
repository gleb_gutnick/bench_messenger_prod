FROM ruby:2.6.9

RUN apt-get update -qq && apt-get install -y build-essential libpq-dev nodejs
RUN gem install bundler:2.3.11

RUN mkdir /chat
WORKDIR /chat

ADD Gemfile /chat/Gemfile
ADD Gemfile.lock /chat/Gemfile.lock

RUN bundle install

ADD . /chat