Rails.application.routes.draw do
  resources :messages
  resources :rooms

  root 'rooms#index'

  get 'search', to: 'rooms#search'

end
