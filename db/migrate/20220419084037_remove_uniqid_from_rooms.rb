class RemoveUniqidFromRooms < ActiveRecord::Migration[6.0]
  def change

    remove_column :rooms, :uniqid, :string
  end
end
