class AddUniqidToRooms < ActiveRecord::Migration[6.0]
  def change
    add_column :rooms, :uniqid, :string
  end
end
