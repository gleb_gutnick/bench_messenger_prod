class Room < ApplicationRecord
  extend FriendlyId
  friendly_id :name, use: :slugged

  
  has_many :messages
  has_many :users, through: :messages
end
